var viz;
var activeSheet;

function initializeViz(sYear, sCountryNames) {
	var sVizURL = "https://public.tableau.com/views/WorldMap-StabilityRatingsfor30Countries/" + sYear;
	var containerDiv = document.getElementById("vizContainer");
	var options;
	if (typeof sCountryNames !== 'undefined') {
		var sFilteredCountries = sCountryNames.toString();		
		//alert(sCountryNames);
		options = {
			hideTabs: false,
			//"Algeria,Argentina,Australia,Brazil,Canada,China,Colombia,Côte d’Ivoire,Ecuador,Egypt,Germany,Greece,India,Indonesia,Jamaica,Japan,Jordan,Kenya,Mexico,Oman,Russian Federation,Singapore,South Africa,Thailand,Turkey,Ukraine,United States,Venezuela,Yemen,Zimbabwe"
			"Country Name": sFilteredCountries,
			onFirstInteractive: function () {
				console.log("Run this code when the viz has finished loading.");
			}
		};
	} else {
		options = {
			hideTabs: false,
			//"Country Name": "United States,Mexico",
			onFirstInteractive: function () {
				console.log("Run this code when the viz has finished loading.");
			}
		};
	}
	
	if (viz) { // If a viz object exists, delete it.
		viz.dispose(); 
	}
			
	viz = new tableau.Viz(containerDiv, sVizURL, options); 
}

function GetUnderlyingData() {
	sheet = viz.getWorkbook().getActiveSheet();
	var jsonData;
	options = {
		maxRows: 0,
		ignoreAliases: false,
		ignoreSelection: true,
		includeAllColumns: false
	};

	sheet.getUnderlyingDataAsync(options).then(function(t){
		table = t;
		$('#divData')[0].innerHTML = "<h4>Underlying Data:</h4><p>" + JSON.stringify(table.getData()) + "</p>";
	});
}

function FilterByThreshold(nValue) {
	sheet = viz.getWorkbook().getActiveSheet();
	var jsonData;
	options = {
		maxRows: 0,
		ignoreAliases: false,
		ignoreSelection: true,
		includeAllColumns: false
	};

	sheet.getUnderlyingDataAsync(options).then(function(t){
		table = t;
		jsonData = table.getData();
		$('#divData')[0].innerHTML = "<h4>Underlying Data:</h4><p>" + JSON.stringify(table.getData()) + "</p>";
		var oFilteredCountriesList = new Array();
		var sCurrentCountryName = '';
		var sCurrentCountryStabilityValue = '';
		for (var i = 0; i < jsonData.length; i++) {
			var obj = jsonData[i];
			for (var key in obj) {
				var attrName = key;
				var attrValue = obj[key];
				for (var tempy in attrValue) {
					if (isNaN(attrValue[tempy])) {
						sCurrentCountryName = attrValue[tempy];
					}
					if (!isNaN(attrValue[tempy])) {
						if (tempy == 'value' && attrValue[tempy] <= nValue) {
							oFilteredCountriesList.push(sCurrentCountryName);	
						}	
					}
				}
				//alert('attrName = ' + attrName + " | obj[key] = " + attrValue);
			}
		}
		var sSelectedYearFilterValue = $('#ddlYearFilter').val();
		initializeViz(sSelectedYearFilterValue, oFilteredCountriesList);
	});
}

function switchView(sheetName) {
	var sDebug = '';	
	var onSuccess = function(filters) {
		//alert('success');
	};
	var onError = function(err) {
		alert('error');
	};
	sheetName = "2002";
	var workbook = viz.getWorkbook().activateSheetAsync("2002")
	.then(function(sheet) {
		activeSheet = sheet;
		sDebug += "activeSheet.getName() = " + activeSheet.getName() + "<br/>";
		sDebug += "activeSheet.getIndex() = " + activeSheet.getIndex() + "<br/>";
		sDebug += "activeSheet.getIsActive() = " + activeSheet.getIsActive() + "<br/>";
		sDebug += "activeSheet.getIsHidden() = " + activeSheet.getIsHidden() + "<br/>";
		sDebug += "activeSheet.getSheetType() = " + activeSheet.getSheetType() + "<br/>";
		sDebug += "activeSheet.getSize() = " + activeSheet.getSize() + "<br/>";
		sDebug += "activeSheet.getUrl() = " + activeSheet.getUrl() + "<br/>";
		sDebug += "activeSheet.getWorkbook() = " + activeSheet.getWorkbook() + "<br/>";
		
		var oDT_SheetData = activeSheet.getSummaryDataAsync();
		sDebug += "oDT_SheetData.getName() = " + oDT_SheetData.getName() + "<br/>";
		sDebug += "oDT_SheetData.getData() = " + oDT_SheetData.getData() + "<br/>";
		sDebug += "oDT_SheetData.getTotalRowCount() = " + oDT_SheetData.getTotalRowCount() + "<br/>";
		sDebug += "oDT_SheetData.getIsSummaryData() = " + oDT_SheetData.getIsSummaryData() + "<br/>";
		
		var oWorkbookTemp = activeSheet.getWorkbook();
		sDebug += "oWorkbookTemp.getActiveSheet() = " + oWorkbookTemp.getActiveSheet() + "<br/>";
		sDebug += "oWorkbookTemp.getActiveCustomView() = " + oWorkbookTemp.getActiveCustomView() + "<br/>";
		sDebug += "oWorkbookTemp.getName() = " + oWorkbookTemp.getName() + "<br/>";
		//$('#divDebug')[0].innerHTML = sDebug;
		
		//activeSheet.getFiltersAsync().then(onSuccess, onError);
		//var sDebug = viz.getWorkbook().getActiveSheet();
	})
	//workbook.activateSheetAsync(sheetName);
	
}

function Debug(obj) {
	str = JSON.stringify(obj);
	str = JSON.stringify(obj, null, 4); // (Optional) beautiful indented output.
	console.log(str); // Logs output to dev tools console.
	alert(str); // Displays output using window.alert()
}